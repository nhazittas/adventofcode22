import sys

def check(n):
    smul = 168630
    bug = 718098

    if n <= 168630:
        return False
    if n >= 718098:
        return False
        
    repeats = False
    increases = True

    s = str(n)

    for i in range(len(s)-1):
        c = int(s[i])
        nex = int(s[i+1])

        if nex < c:
            return False
        if c == nex:
            repeats = True

    return repeats

for i in range(3340,3500):
    if check(i):
        print(i)



